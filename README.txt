1. Put 'requisition' folder into your webroot folder.

2. Create a new database (i.e: 'requisition_system'), import DB from '/requisition/config/schema/requisition_system.sql' into that DB.

3. Configure database settings in 'requisition/config/database.php':

<?php

    class DB_CONFIG {

        public $default = array(
            'host' => 'localhost',
            'user' => 'root',
            'password' => '',
            'database' => 'requisition_system'
        );

    }
?>

Replace those information (host, user, password, database) above by yours.

4. Use the URL: 'http://localhost/requisition/' to start (or if you prefer virtual host, set document root to requisition/ and use your defined host).

* On Mac/Linux, if you have 'Permission denied' error, change permision to 755 by entering this command in terminal:

	sudo chmod -R 755 [PATH_TO_WEBROOT]/requisition/





