<?php
require_once('config/database.php');

class Db {
    private static $instance = NULL;
    private static $config = NULL;
    private function __construct() {}

    public static function getInstance() {
        if (!isset(self::$instance)) {
            $pdo_options[PDO::ATTR_ERRMODE] = PDO::ERRMODE_EXCEPTION;

            self::$config = new DB_CONFIG();

            self::$instance = new PDO("mysql:host=".self::$config->default['host'].";dbname=".self::$config->default['database'],
                                      self::$config->default['user'], self::$config->default['password'], $pdo_options);
        }
        return self::$instance;
    }
}
?>