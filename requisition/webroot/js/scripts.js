var targetErr = $('#errMsg');
$(document).ready(function($) {

    loadList();

    $("#frmRequisition").submit(function( event ) {
        event.preventDefault();
        var targetUrl = window.location.origin + window.location.pathname + '?controller=RequisitionService&action=add';
        if (!$("#txtMaterial").val()) {
            targetErr.html('Material name is required');
            return;
        }
        if (!$("#txtColour").val()) {
            targetErr.html('Colour name is required');
            return;
        }
        if (!$("#txtQuantity").val()) {
            targetErr.html('Quantity is required');
            return;
        }

        if (!isInt($("#txtQuantity").val()) || $("#txtQuantity").val() < 0 ) {
            targetErr.html('Quantity must be a positive integer');
            return;
        }

        $.ajax({
            url: targetUrl,
            type: 'post',
            dataType: 'json',
            data:{postData:{material:$("#txtMaterial").val(), colour:$("#txtColour").val(), quantity:$("#txtQuantity").val(), comments:$("#txtComment").val()}},
            success: function(res) {
                if (res.status == 1){
                    targetErr.html('');
                    loadList();
                } else {
                    targetErr.html(res.msg);
                }
            },
            error: function(err) {
                alert(err);
            }
        });
    });
});

//loadList
function loadList(){
    var targetUrl = window.location.origin + window.location.pathname + '?controller=RequisitionService&action=load';
    var targetHtml = $('#tblRequisition > tbody' );

    $.ajax({
        url: targetUrl,
        type: 'post',
        dataType: 'json',
        success: function(data) {

            if (data.status == 1){
                targetHtml.html('');
                targetErr.html('');
                if (data.data.length) {
                    newHtml = '';
                    $.each(data.data , function (k,req){
                        newHtml = '<td>' + req.createdDate + '</td>' +
                            '<td>' + req.m_name + '</td>' + '<td>' + req.quantity + '</td>' +
                            '<td>' + req.c_name + '</td>' + '<td bgcolor=#' + req.R + req.G + req.B + '></td>';
                        targetHtml.append('<tr>' + newHtml + '</td>');
                    });
                }

            } else {
                targetErr.html(data.msg);
            }
        }
    });
}

function isInt(value) {
    var x = parseFloat(value);
    return !isNaN(value) && (x | 0) === x;
}
