<?php

    function call($controller, $action) {
        require_once('Controller/' . $controller . '.php');

        switch($controller) {
            case 'RequisitionService':
                $controller = new RequisitionServiceController();
                break;
        }

        $controller->{ $action }();
    }

    $controller_list = array(
            'RequisitionService' => ['index','load', 'add'],
            'Page' => ['error404']);

    if (array_key_exists($controller, $controller_list)) {
        if (in_array($action, $controller_list[$controller])) {
            call($controller, $action);
        }
    }

?>