<?php
require_once('Model/Requisition.php');
require_once('Model/Material.php');
require_once('Model/Colour.php');
class RequisitionServiceController {


    public function index() {
        require_once('View/RequisitionService/index.php');
    }

    public function load() {
        $req = Requisition::all();
        $result = array('status' => 1, 'msg' => 'Load data successfully', 'data' => $req);
        echo json_encode($result);
    }

    public function add() {
        $addFields = array('material','colour','quantity');
        $dtAdd = array();

        $result = array('status' => 0, 'msg' => '', 'data' => '');
        if (isset($_REQUEST['postData'])) {
            $postData = $_REQUEST['postData'];

            foreach ($addFields as $field) {
                if (!isset($postData[$field]) || empty($postData[$field])) {
                    $result['status'] = 0;
                    $result['msg'] = "$field is required";
                    echo json_encode($result);
                    return;
                }
            }
            $dtAdd['quantity'] = $postData['quantity'];
            $dtAdd['comments'] = $postData['comments'];

            $materialId = Material::getIdByName($postData['material']);
            if (empty($materialId)) {
                $result['status'] = 0;
                $result['msg'] = "Material name is not valid";
                echo json_encode($result);
                return;
            }

            $colourId = Colour::getIdByName($postData['colour']);
            if (empty($colourId)) {
                $result['status'] = 0;
                $result['msg'] = "Colour name is not valid";
                echo json_encode($result);
                return;
            }

            $dtAdd['material'] = $materialId;
            $dtAdd['colour'] = $colourId;
            $dtAdd['createdDate'] = date('Y-m-d');

            $addRe = Requisition::add($dtAdd);

            if ($addRe) {
                $result['status'] = 1;
                $result['msg'] = "Data added successfully";
                echo json_encode($result);
                return;
            } else {
                $result['status'] = 0;
                $result['msg'] = "Can not add data";
                echo json_encode($result);
                return;
            }

        } else {
            $result['status'] = 0;
            $result['msg'] = 'No post data';
            echo json_encode($result);
            return;
        }


    }


}
?>