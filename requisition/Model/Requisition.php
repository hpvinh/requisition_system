<?php

class Requisition {

    public static function all() {
        $db = Db::getInstance();
        $data = $db->query('SELECT Requisition.idRequisition, DATE_FORMAT(Requisition.createdDate, "%D %M %Y") as createdDate, Requisition.quantity,
                                    Material.name as m_name, Colour.name as c_name, Colour.R, Colour.G, Colour.B FROM Requisition
                            Left Join Material on Material.idMaterial = Requisition.material
                            Left Join Colour on Colour.idColour = Requisition.colour');


        return $data->fetchAll();
    }

    public static function add($data) {
        try {
            $db = Db::getInstance();
            $req = $db->prepare('INSERT INTO Requisition (createdDate,material,colour,quantity,comments)
                              VALUES (:createdDate,:material,:colour,:quantity,:comments)');
            if ($req->execute($data)){
                return $db->lastInsertId();
            }
            return false;
        } catch (Exception $e){
            return $e;
        }

    }

}
?>