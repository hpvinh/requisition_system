<?php

    class Material {

        public static function getIdByName($name) {
            if (!empty($name)) {
                $db = Db::getInstance();
                // we make sure $id is an integer
                $req = $db->prepare('SELECT idMaterial FROM Material WHERE name = :name');
                // the query was prepared, now we replace :id with our actual $id value
                $req->execute(array('name' => $name));
                $material = $req->fetch();

                return $material['idMaterial'];
            }
            return false;
        }
    }
?>