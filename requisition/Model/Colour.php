<?php

    class Colour {

        public static function getIdByName($name) {
            if (!empty($name)) {
                $db = Db::getInstance();
                // we make sure $id is an integer
                $req = $db->prepare('SELECT idColour FROM Colour WHERE name = :name');
                // the query was prepared, now we replace :id with our actual $id value
                $req->execute(array('name' => $name));
                $colour = $req->fetch();
                return $colour['idColour'];
            }
            return false;
        }
    }
?>