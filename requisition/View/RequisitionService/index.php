<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>Requisition</title>

	<meta name="description" content="Source code generated using layoutit.com">
	<meta name="author" content="LayoutIt!">

	<link href="webroot/css/bootstrap.min.css" rel="stylesheet">
	<link href="webroot/css/style.css" rel="stylesheet">

</head>
<body>

<div class="container-fluid">
	<div class="row">
		<div id="listRequisition" class="col-md-12">
			<form id="frmRequisition" role="form" >
				<div class="form-group">

					<label for="txtMaterial">
						A material mame
					</label>
					<input type="text" class="form-control" id="txtMaterial">
				</div>
				<div class="form-group">
					<label for="txtColour">
						A colour name
					</label>
					<input type="text" class="form-control" id="txtColour">
				</div>
				<div class="form-group">
					<label for="txtQuantity">
						A quantity
					</label>
					<input type="text" class="form-control" id="txtQuantity">
				</div>
				<div class="form-group">
					<label for="txtComment">
						A comment
					</label>
					<input type="text" class="form-control" id="txtComment">
				</div>
				<div class="form-group">
					<button type="submit" class="btn btn-default" id="btnSave">
						Save
					</button>
				</div>
			</form>
			<br/>
			<div id="errMsg" style="color: red"></div>
			<br/>
			<table id="tblRequisition" class="table table-bordered">
				<thead>
				<tr>
					<th>
						Date
					</th>
					<th>
						Material
					</th>
					<th>
						Quantity in Sheets
					</th>
					<th>
						Colour
					</th>
					<th>
						Sample
					</th>
				</tr>
				</thead>
				<tbody>
				</tbody>
			</table>
		</div>
	</div>
</div>

<script src="webroot/js/jquery.min.js"></script>
<script src="webroot/js/bootstrap.min.js"></script>
<script src="webroot/js/scripts.js"></script>
</body>
</html>