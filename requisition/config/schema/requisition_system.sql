
--
-- Database: `requisition_system`
--

-- --------------------------------------------------------

--
-- Table structure for table `Colour`
--

CREATE TABLE IF NOT EXISTS `Colour` (
  `idColour` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(255) NOT NULL,
  `R` varchar(2) NOT NULL,
  `G` varchar(2) NOT NULL,
  `B` varchar(2) NOT NULL,
  PRIMARY KEY (`idColour`),
  UNIQUE KEY `Name` (`Name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=17 ;

--
-- Dumping data for table `Colour`
--

INSERT INTO `Colour` (`idColour`, `Name`, `R`, `G`, `B`) VALUES
(1, 'Black', '00', '00', '00'),
(2, 'Silver', 'C0', 'C0', 'C0'),
(3, 'Grey', '80', '80', '80'),
(4, 'White', 'FF', 'FF', 'FF'),
(5, 'Maroon', '80', '00', '00'),
(6, 'Red', 'FF', '00', '00'),
(7, 'Purple', '80', '00', '80'),
(8, 'Fuschia', 'FF', '00', 'FF'),
(9, 'Green', '00', '80', '00'),
(10, 'Lime', '00', 'FF', '00'),
(11, 'Olive', '80', '80', '00'),
(12, 'Yellow', 'FF', 'FF', '00'),
(13, 'Navy', '00', '00', '80'),
(14, 'Blue', '00', '00', 'FF'),
(15, 'Teal', '00', '80', '80'),
(16, 'Aqua', '00', 'FF', 'FF');

-- --------------------------------------------------------

--
-- Table structure for table `Material`
--

CREATE TABLE IF NOT EXISTS `Material` (
  `idMaterial` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`idMaterial`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `Material`
--

INSERT INTO `Material` (`idMaterial`, `name`) VALUES
(2, '120gm Card'),
(1, '80gm Card'),
(3, 'Art Card'),
(6, 'Heavy Plastic Card'),
(5, 'Plastic Card'),
(4, 'Vis Card');

-- --------------------------------------------------------

--
-- Table structure for table `Requisition`
--

CREATE TABLE IF NOT EXISTS `Requisition` (
  `idRequisition` int(11) NOT NULL AUTO_INCREMENT,
  `createdDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `material` int(11) NOT NULL,
  `colour` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `comments` varchar(2000) DEFAULT NULL,
  PRIMARY KEY (`idRequisition`),
  KEY `material` (`material`),
  KEY `colour` (`colour`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `Requisition`
--

INSERT INTO `Requisition` (`idRequisition`, `createdDate`, `material`, `colour`, `quantity`, `comments`) VALUES
(1, '2015-10-08 00:00:00', 1, 8, 200, 'Good');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `Requisition`
--
ALTER TABLE `Requisition`
  ADD CONSTRAINT `requisition_ibfk_1` FOREIGN KEY (`material`) REFERENCES `Material` (`idMaterial`) ON UPDATE CASCADE,
  ADD CONSTRAINT `requisition_ibfk_2` FOREIGN KEY (`colour`) REFERENCES `Colour` (`idColour`) ON UPDATE CASCADE;
